package com.dc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion de la Persona")
@Table(name = "persona")
@Entity
public class Persona {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPersona;
	
	@ApiModelProperty(notes = "Debe de tener almenos 3 caracteres el campo nombre")
	@Size(min = 3, message = "Debe de tener almenos 3 caracteres el campo nombre")
	@Column(name = "nombres", length = 90, nullable = false)
	private String nombres;
	
	@ApiModelProperty(notes = "Debe de tener almenos 3 caracteres los apellidos")
	@Size(min = 3, message = "Debe de tener almenos 3 caracteres los apellidos")
	@Column(name = "apellidos", length = 90, nullable = false)
	private String apellidos;

	public Integer getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	

}
