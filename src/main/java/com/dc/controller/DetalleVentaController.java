package com.dc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dc.exception.ModeloNotFoundException;
import com.dc.model.DetalleVenta;
import com.dc.service.IDetalleVentaService;

@RestController
@RequestMapping("/detalleVentas")
public class DetalleVentaController {

	@Autowired
	private IDetalleVentaService service;

	@GetMapping
	public ResponseEntity<List<DetalleVenta>> listar() {
		List<DetalleVenta> lista = service.listar();
		return new ResponseEntity<List<DetalleVenta>>(lista, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<DetalleVenta> listarPorId(@PathVariable("id") Integer id) {
		DetalleVenta detalleVenta = service.leerPorId(id);
		if (detalleVenta.getIdDetalleVenta() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<DetalleVenta>(detalleVenta, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<DetalleVenta> registrar(@Valid @RequestBody DetalleVenta obj) {
		DetalleVenta DetalleVenta = service.registrar(obj);
		return new ResponseEntity<DetalleVenta>(DetalleVenta, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<DetalleVenta> modificar(@Valid @RequestBody DetalleVenta obj) {
		DetalleVenta DetalleVenta = service.registrar(obj);
		return new ResponseEntity<DetalleVenta>(DetalleVenta, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
		DetalleVenta DetalleVenta = service.leerPorId(id);
		if (DetalleVenta.getIdDetalleVenta() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
