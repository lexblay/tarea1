package com.dc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dc.model.DetalleVenta;

public interface IDetalleVentaRepo extends JpaRepository<DetalleVenta, Integer> {

}
