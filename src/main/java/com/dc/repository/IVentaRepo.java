package com.dc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dc.model.Venta;

public interface IVentaRepo extends JpaRepository<Venta, Integer> {

}
