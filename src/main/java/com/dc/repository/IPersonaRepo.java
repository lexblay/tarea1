package com.dc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dc.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer> {

}
