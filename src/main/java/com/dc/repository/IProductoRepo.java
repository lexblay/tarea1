package com.dc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dc.model.Producto;

public interface IProductoRepo  extends JpaRepository<Producto, Integer>{

}
