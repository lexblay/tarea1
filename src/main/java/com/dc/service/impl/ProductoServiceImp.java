package com.dc.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dc.model.Producto;
import com.dc.repository.IProductoRepo;
import com.dc.service.IProductoService;

@Service
public class ProductoServiceImp implements IProductoService {

	@Autowired
	IProductoRepo prod;
	
	@Override
	public Producto registrar(Producto obj) {
		return prod.save(obj);		
	}

	@Override
	public Producto modificar(Producto obj) {
		return prod.save(obj);
	}

	@Override
	public List<Producto> listar() {
		return prod.findAll();
	}

	@Override
	public Producto leerPorId(Integer id) {
		Optional<Producto>  op = prod.findById(id);
		return op.isPresent() ? op.get() : new Producto();		
	}

	@Override
	public void eliminar(Integer id) {
		prod.deleteById(id);
		
	}

}
