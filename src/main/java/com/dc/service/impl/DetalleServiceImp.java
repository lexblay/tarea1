package com.dc.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dc.model.DetalleVenta;
import com.dc.repository.IDetalleVentaRepo;
import com.dc.service.IDetalleVentaService;

@Service
public class DetalleServiceImp implements IDetalleVentaService{

	@Autowired
	private IDetalleVentaRepo repo;
	
	@Override
	public DetalleVenta registrar(DetalleVenta obj) {
		return repo.save(obj);
	}

	@Override
	public DetalleVenta modificar(DetalleVenta obj) {
		return repo.save(obj);
	}

	@Override
	public List<DetalleVenta> listar() {
		return repo.findAll();
	}

	@Override
	public DetalleVenta leerPorId(Integer id) {
		Optional<DetalleVenta> op = repo.findById(id);
		return op.isPresent() ? op.get() : new DetalleVenta();
	}

	@Override
	public void eliminar(Integer id) {
		repo.deleteById(id);	
	}

}
